// First..

const express = require ('express');

// Create an application with expressjs, like createServer
const app = express(); 

// designate port number to server
const port = 4000;


//middleware
//express.json() is a method which allows us to handle the streaming of data and automatically parse (unlike step1 and step2 in node)
//for expressjs api
app.use(express.json());

let users = [
	{
		username: "Bmadrigal",
		email: "fateReader@gmail.com",
		password: "weDontTalkAboute"
	},
	{
		username: "LuisaMadrigal",
		email: "strongSis@gmail.com",
		password:"pressure"
	}
];

let items = [
	{
		product: "roses",
		price: 170,
		isActive: true
	},
	{
		product: "tulips",
		price: 250,
		isActive: true
	}
	];


// routing methods

app.get('/', (req, res) => {
	res.send('Hello from ExpressJS Api!')
});

app.get('/greeting', (req, res) => {
	res.send('Hello from Batch 169')
});


app.get('/users', (req, res) => {
	res.send(users);
});

//how do we get date from the client as a request body?

app.post('/users', (req, res) => {
	console.log(req.body)
	let newUser = {
		username: req.body.username,
		email: req.body.email,
		password: req.body.password
	}

	users.push(newUser);
	console.log(users);
	res.send(users);
});


app.delete('/users', (req, res) => {
	users.pop();
	console.log('users')
	res.send(users)
});


// updating users

//wild card -for specificity
app.put('/users/:index', (req,res) => {

//will contain updated password
	console.log(req.body)

// object which contains the value and url params 
// url params is captured by route parameters  (:parameterName) and saved as property in req.params
	console.log(req.params)

	// parseInt the value of the number coming from req.params
	let index = parseInt(req.params.index);
	
	// get the user that we want to update with our index number from url params
	users[index].password =req.body.password

	// send the updated user to the client
	// provide the index variable to be the index for the particular item in the array
	res.send(users[index]);
});

app.get('/users/getSingleUser/:index', (req, res) => {
	let index = parseInt(req.params.index);
	res.send(users[index]);
});

/*
	Mini Activity

	>> Create a new route to get and send items array in the client

	>> Create a new route to create and add a new item object in the items array
		>> send the updated items array in the client
		>> check the post method route for our users for reference

	>> Create a new route which can update the price of a single item in the array
		>> pass the index number of the item that you want to update in the request body
		>> add the index number of the item you want to update to access it and its price property
		>> reassign the new price from our request body
		>> send the updated item in the client

	>> Send your Postman outputs in hangouts

*/



app.get('/items', (req, res) => {
	res.send(items);
});


app.post('/items', (req, res) => {
	console.log(req.body)
	let newItem = {
		product: req.body.product,
		price: req.body.price,
		isActive: req.body.isActive
	}
	items.push(newItem);
	console.log(items);
	res.send(items);
});


app.put('/items/:index', (req, res) => {
	console.log(req.body)
	console.log(req.params)
	let index = parseInt(req.params.index);
	items[index].price = 500
	res.send(items[index]);
});

// activity
/*
>> Create a new route with '/items/getSingleItem/:index' endpoint. 
	>> This route should allow us to GET the details of a single item.
		-Pass the index number of the item you want to get via the url as url params in your postman.
		-http://localhost:4000/items/getSingleItem/<indexNumberOfItem>
	>> Send the request and back to your api/express:
		-get the data from the url params.
		-send the particular item from our array using its index to the client.

>> Create a new route to update an item with '/items/archive/:index' endpoint.	
	>> This route should allow us to UPDATE the isActive property of our product.
		-Pass the index number of the item you want to de-activate via the url as url params.
		-Access the particular item with its index. Access the isActive property of the item and re-assign it to false.
		-send the updated item in the client

>> Create a new route to update an item with '/items/activate/:index' endpoint. 
	>> This route should allow us to UPDATE the isActive property of our product.
		-Pass the index number of the item you want to activate via the url as url params.
		-Access the particular item with its index. Access the isActive property of the item and re-assign it to true.
		-send the updated item in the client

>> Save the Postman collection in your s28-s29 folder

*/

//1. get single item
app.get('/items/getSingleItem/:index', (req, res) => {
	// parseInt to convert index to an number
	let index = parseInt(req.params.index);
	res.send(items[index]);
});

// 2. archive -soft delete
	// endpoint: '/items/archive/:index'
app.put('/items/archive/:index', (req, res) => {
	console.log(req.params)
	let index = parseInt(req.params.index);
	// reassign to false directly, not thru req.body
	items[index].isActive = false;
	res.send(items[index]);
});

// 2. activate
	// endpoint: '/items/activate/:index'
app.put('/items/activate/:index', (req, res) => {
	console.log(req.params)
	let index = parseInt(req.params.index);
	items[index].isActive = true
	res.send(items[index]);
});


// listen--like node
app.listen(port, () => console.log (`Server is running at port ${port}`))

